PRTG OIDLib for Eltek devices
===========================================

This project contains all the files necessary to create library sensors the Eltek 
devices based on the Eltek-Distributed MIB.


Donwload Instructions
======================
Please use this link to download [Eltek OIDLib Install package](https://gitlab.com/PRTG/OIDLibs/Eltek/-/jobs/artifacts/master/download?job=PRTGDistZip)


PRTG OIDLibs
======================

PRTG Network monitor can create sensors using Library sensors using OIDLib imported
from MIB files providecd by the Manufacturer.

The OIDLib files lists all available OID's, their decription and other attributes.

The Import is done using the [Paessler MIB Importer](https://www.paessler.com/tools/mibimporter) available from the Paessler website.


#### Installation Instructions

The OIDLib project has a standard directory structure:
All the files in the PRTG subdirectory needs to go into the PRTG program directory
(https://kb.paessler.com/en/topic/463-how-and-where-does-prtg-store-its-data).
This means, copy the files from "prtg" to "%programfiles(x86)%\PRTG Network Monitor" and the 
files will be copied into the correct locations in the subdirectories. 
Please ** NOTE **, the destination "%programfiles(x86)%" is a "protected path " and requires elevated privileges.
(IE if you copy across the network, the "admin user may not have the correct drive mappings).

<div class="panel panel-info">
Directory Layout:

<div class="panel-body">
<pre>
 ProjectName
   + - traces		         (files to use for testing, SNMP traces etc.)
   + - PRTG                  (hierarchy that goes into the PRTG directory)
        + - Custom Sensors	 (Where PRTG stores Custom senosrs)
        + - devicetemplates  (Where PRTG stores the device templates)
        + - lookups          (Where PRTG stores the lookups)
            + - custom       (Where PRTG stores custom lookups)
        + - MIB              (Where PRTG stores MIBs)
        + - notifications	 (Where PRTG stores Custom Notifications)
        + - snmplibs         (Where PRTG stores imported custom OID Libraries)
        + - webroot          (PRTG webgui)
            + - icons        (PRTG webgui icons)
                + - devices  (PRTG device icons)
</pre>
</div>
</div>
If there are any MIB files in the package, they will not be loaded until PRTG is rebooted.
Lookups can be manually re-loaded by using the the Admin Tools page "Setup -> System Administration -> Administrative Tools"
or: https://{Your_PRTG_Server_IP_and_Port}/api/loadlookups.htm

### Included Files

#### lookups\custom (xxx.ovl) 
PRTG lookup files for use with sensors with lookup values

#### notifications
Custom PRTG Notifications.

#### MIB
Manufacturer supplied MIB files to include. These will be loaded into PRTG Core server on startup.

#### snmplibs (xxx.oidlib)
Files imported from MIB files by the [Paessler MIB importer](https://www.paessler.com/tools/mibimporter).

#### webroot\icons\devices (vendors_xxx.png/svg)
Icon files used by PRTG to display in the tree. 
 -PNG's: Should be 14.14 pixels.
 -SVG's; Should be small images for best presentation 
 




